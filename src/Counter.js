import React, { useState } from 'react'

const Display = ({ counter }) => <div>{counter}</div> 

const Button = ({ text, handleClick }) => <button onClick={handleClick}>{text}</button>

const Counter = () => {

  const [ counter, setCounter ] = useState(0)

  const increaseByOne = () => setCounter(counter+1)
  const decreaseByOne = () => setCounter(counter-1)
  const setToZero = () => setCounter(0)
  
  return (
    <div>
      <Display counter={counter} />
      <Button text={'Minus'} handleClick={decreaseByOne} />
      <Button text={'Zero'} handleClick={setToZero} />
      <Button text={'Plus'} handleClick={increaseByOne} />
    </div>
  )
}

export default Counter